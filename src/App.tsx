import React from 'react';
import { Routes, Route } from "react-router-dom";
import Main from './pages/Main/Main';
import ImagePage from './pages/ImagePage/ImagePage';
import './assets/styles/variables.css';
import './App.css';

function App() {
	return (
    <Routes>
      <Route path="/" element={<Main />} />
      <Route path="/:id" element={<ImagePage />} />
    </Routes>
	);
}

export default App;
