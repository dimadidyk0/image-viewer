import  { useEffect, useState } from 'react';
import { API_URL } from '../constants/api';
import { ImageEntity } from '../types/image';

export function useFetchImages(): { data: ImageEntity[]; isLoading: boolean, error: null | Object } {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [images, setImages] = useState([]);

  useEffect(() => {
    setIsLoading(true);

    fetch(API_URL)
      .then((r) => r.json())
      .then((data) => {
        setImages(data);
        setIsLoading(false);
      })
      .catch(err => setError(err));
  }, []);

  return { data: images, isLoading, error }
}

export function useFetchImageItem(id: string | undefined): { data: ImageEntity | null; isLoading: boolean, error: null | Object } {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [image, setImage] = useState(null);

  useEffect(() => {
    setIsLoading(true);

    fetch(`${API_URL}/${id}`)
      .then((r) => r.json())
      .then((data) => {
        setImage(data);
        setIsLoading(false);
      })
      .catch(err => setError(err));
  }, [id]);

  return { data: image, isLoading, error }
}

export default useFetchImages;