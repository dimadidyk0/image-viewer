import React from 'react';
import styles from './SearchInput.module.css';

type Props = {
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  value: string;
};

export function SearchInput({ onChange, value }: Props) {
  return (
    <div className={styles.root}>
      <input placeholder="Photo title..." className={styles.input} onChange={onChange} value={value} />  
    </div>
  );
}

export default SearchInput;
