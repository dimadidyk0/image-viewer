import React, { useState, useCallback } from 'react';
import { Link } from 'react-router-dom';
import Modal from '../Modal/Modal';
import { ImageEntity } from '../../types/image';
import { PHOTO_ITEM } from '../../constants/routes';
import searchIconSrc from '../../assets/img/search.png';
import styles from './ImageCard.module.css';

type Props = {
  image: ImageEntity;
  style?: Object;
  onRemove: (id: number) => void;
};

export function ImageCard({ image, style, onRemove }: Props) {
  const [showModal, setShowModal] = useState(false);
  const handleIncreaseBtnClick = useCallback(() => {
    setShowModal(true);
  }, []);
  const handleCloseModal = useCallback(() => {
    setShowModal(false);
  }, []);
  const handleRemove = useCallback(() => {
    onRemove(image.id);
  }, [onRemove, image.id]);

  if (!image) {
    return null;
  }

  return (
    <div className={styles.root} style={style}>
      <div className={styles.imgContainer}>
        <img
          alt=""
          src={image.thumbnailUrl}
          className={styles.img}
        />

        <button onClick={handleIncreaseBtnClick} className={styles.increaseBtn}>
          <img alt="" src={searchIconSrc} />
        </button>
        <button className={styles.remove} onClick={handleRemove}>X</button>
      </div>

      <Link to={PHOTO_ITEM.replace(':id', `${image.id}`)} className={styles.title}>{image.title}</Link>

      <Modal
        isOpen={showModal} 
        className={styles.modal}
        overlayClassName={styles.overlay}
        onRequestClose={handleCloseModal}
      >
        <img alt={image.title} src={image.url} />
      </Modal>
  </div>
  );
}

export default ImageCard;
