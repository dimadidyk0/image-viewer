import React from 'react';
import { Link } from 'react-router-dom';
import { ImageEntity } from '../../types/image';
import { PHOTOS } from '../../constants/routes';
import styles from './ImageProfile.module.css';

type Props = {
  photo: ImageEntity | null;
};

export function ImageProfile({ photo }: Props) {
  if (!photo) {
    return null;
  }

  return (
    <div className={styles.root} >
      <img
        src={photo.url}
        className={styles.img}
        alt={photo.title}
        title={photo.title}
      />

      <h4 className={styles.text}>Title: <strong>{photo.title}</strong></h4>
      <p className={styles.text}>Album ID: <strong>{photo.albumId}</strong></p>
      <p className={styles.text}>Photo ID: <strong>{photo.id}</strong></p>

      <Link className={styles.link} to={PHOTOS}>Go back</Link>
  </div>
  );
}

export default ImageProfile;
