import { useMemo } from 'react';
import {
  CellMeasurer,
  CellMeasurerCache,
  createMasonryCellPositioner,
  Masonry,
  WindowScroller,
} from 'react-virtualized';
import ImageCard from '../ImageCard/ImageCard';

function ImageGrid({ images, onRemove }) {
  const cache = useMemo(() => new CellMeasurerCache({
    defaultHeight: 170,
    defaultWidth: 150,
    fixedWidth: true,
  }), []);

  const cellPositioner = useMemo(() => createMasonryCellPositioner({
    cellMeasurerCache: cache,
    columnCount: 8,
    columnWidth: 150,
    spacer: 10,
  }), [cache]);

  function cellRenderer({ index, key, parent, style }) {
    const image = images[index];

    return (
      <CellMeasurer cache={cache} index={index} key={key} parent={parent}>
        <ImageCard onRemove={onRemove} style={style} image={image} />
      </CellMeasurer>
    );
  }

  return (
    <WindowScroller>
      {({ height, isScrolling, scrollTop }) => (
        <Masonry
          cellCount={images.length}
          cellMeasurerCache={cache}
          cellPositioner={cellPositioner}
          cellRenderer={cellRenderer}
          width={1280}
          autoHeight
          height={height}
          isScrolling={isScrolling}
          scrollTop={scrollTop}
        />
      )}
    </WindowScroller>
  )
}

export default ImageGrid;
