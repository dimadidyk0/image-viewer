import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';
import { useRefEffect } from '../../hooks/useRefEffect';
import ModalPortal from './ModalPortal/ModalPortal';
import styles from './Modal.module.css';

const Modal = ({
  isOpen,
  className,
  onRequestClose,
  overlayClassName,
  rootClassName,
  children,
  shouldDisableScroll,
}) => {
  const modalRef = useRefEffect((node) => {
    if (isOpen && shouldDisableScroll) {
      disableBodyScroll(node);
    }
    return () => enableBodyScroll(node);
  });

  return !isOpen || !children ? null : (
    <ModalPortal rootClassName={rootClassName}>
      <div
        onClick={onRequestClose}
        className={classNames(styles.overlay, overlayClassName)}
      >
        <div
          ref={modalRef}
          className={classNames(styles.modal, className)}
          onClick={(e) => e.stopPropagation()}
        >
          {children}
        </div>
      </div>
    </ModalPortal>
  );
};

Modal.propTypes = {
  isOpen: PropTypes.bool,
  className: PropTypes.string,
  onRequestClose: PropTypes.func,
  rootClassName: PropTypes.string,
  overlayClassName: PropTypes.string,
  children: PropTypes.node,
  shouldDisableScroll: PropTypes.bool,
};

Modal.defaultProps = {
  isOpen: false,
  rootClassName: '',
  shouldDisableScroll: true,
};

export default Modal;
