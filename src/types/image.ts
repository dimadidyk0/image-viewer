export type ImageEntity = {
  albumId: number;
  id: number;
  thumbnailUrl: string;
  title: string;
  url: string;
};
