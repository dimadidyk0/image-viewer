import { useParams } from 'react-router-dom';
import { useFetchImageItem } from '../../hooks/useFetchImages';
import ImageProfile from '../../components/ImageProfile/ImageProfile';
import styles from './ImagePage.module.css';

export function ImagePage() {
  const { id } = useParams()
  const { data, isLoading, error } = useFetchImageItem(id);

  if (isLoading) {
    return <>Loading...</>;
  }

  if (error) {
    return <div>There is an unexpected error</div>;
  }

  return (
    <div className={styles.root}>
      <ImageProfile photo={data} />
    </div>
  );
}

export default ImagePage;