import { useState, useCallback, useMemo } from 'react';
import ImageGrid from '../../components/ImageGrid/ImageGrid';
import SearchInput from '../../components/SearchInput/SearchInput';
import { useFetchImages } from '../../hooks/useFetchImages';
import { ImageEntity } from '../../types/image';
import styles from './Main.module.css';

export function Main() {
  const { data, isLoading, error } = useFetchImages();
  const [query, setQuery] = useState('');
  const [removedPhotos, setRemovedPhotos] = useState<Array<number>>([]);

  const handleInputChange = useCallback((event: React.ChangeEvent<HTMLInputElement>): void => {
    setQuery(event?.target?.value ?? '')
  }, []);

  const handleRemovePhoto = useCallback((id: number) => {
    setRemovedPhotos(prevPhotos => ([...prevPhotos, id]));
  }, []);

  const filteredList = useMemo(() => {
    return data.filter((imageData: ImageEntity) => (
      imageData.title.toLowerCase().includes(query.toLowerCase())
        && !removedPhotos.includes(imageData.id)
    ));
  }, [query, data, removedPhotos]);

  if (isLoading) {
    return <>Loading...</>;
  }

  if (error) {
    return <div>There is an unexpected error</div>;
  }

  return (
    <div className={styles.root}>
      <SearchInput onChange={handleInputChange} value={query} />
      <ImageGrid images={filteredList} onRemove={handleRemovePhoto} />
    </div>
  );
}

export default Main;